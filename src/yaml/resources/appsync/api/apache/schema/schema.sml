type Api {
	repository: Repository
}

type Maintainer {
	name: String
	mbox: String
}

type Project {
	name: String
	description: String
	shortdesc: String
	homepage: String
	download_page: String
	bug_database: String
	mailing_list: String
	license: String
	repository: [String]
	category: String
	created: String
	implements: String
	programming_language: String
	release: [Release]
	doap: String
	maintainer: [Maintainer]
}

type Query {
	apache: Api
}

type Release {
	name: String
	created: String
	revision: String
}

type Repository {
	projects(name: String): [Project]
}

schema {
	query: Query
}
